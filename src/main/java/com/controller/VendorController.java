package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.dao.VendorDAO;
import com.dao.VendorDAOImpl;
import com.model.Vendor;


@Controller
public class VendorController {
	VendorDAO dao= new VendorDAOImpl();
	@RequestMapping("vendorRegister")
	public ModelAndView vendorRegister() {
	return new ModelAndView("vendorRegister","e",new Vendor());
	}
	@RequestMapping("vendorAdd")
	public ModelAndView add(@ModelAttribute("e") Vendor e) {
		dao.insertVendor(e);
		return new ModelAndView("display");
	}
@RequestMapping("vendorLogin")
public ModelAndView vendorLogin()
{
	return new ModelAndView("vendorLogin","e",new Vendor());
}
    @RequestMapping("vendorValidate")
    public ModelAndView vendorValidate(@ModelAttribute("e") Vendor vendor)
{
    	boolean isValid = dao.vendorAuthentication(vendor);
    	if(isValid) {
    	return new ModelAndView("vendorLogin", "v", "Login Successfully");
    	} else {
    		return new ModelAndView("vendorLogin", "v", "Check your Credentials");
    	}
}
}
