package com.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;

import com.model.Policy;

@Component
public class PolicyDao {
	
	@Autowired
	private HibernateTemplate hibernateTemplate;

	// create
	@Transactional
	public void createPolicy(Policy policy) {

		this.hibernateTemplate.saveOrUpdate(policy);

	}

	// get all products
	public List<Policy> getPolicies() {
		List<Policy> policies = this.hibernateTemplate.loadAll(Policy.class);
		return policies;
	}

	// delete the single product
	@Transactional
	public void deletePolicy(int pid) {
		Policy p = this.hibernateTemplate.load(Policy.class, pid);
		this.hibernateTemplate.delete(p);
	}

	// get the single product
	public Policy getPolicy(int pid) {
		return this.hibernateTemplate.get(Policy.class, pid);
	}

}
