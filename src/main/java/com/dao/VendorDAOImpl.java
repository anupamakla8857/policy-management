package com.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.model.Vendor;

public class VendorDAOImpl implements VendorDAO{

	public void insertVendor(Vendor vendor) {
		Session session=new Configuration().configure("admincfg.xml").buildSessionFactory().getCurrentSession();
		
		Transaction tx=session.beginTransaction();
		session.save(vendor);
		tx.commit();
	}

	@SuppressWarnings("rawtypes")
	public boolean vendorAuthentication(Vendor vendor) {
		boolean isValid=false;
		Session session=new Configuration().configure("admincfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query q=session.createQuery("from Vendor where vid=?1 and password=?2");
		q.setParameter(1,vendor.getVid());
		q.setParameter(2,vendor.getPassword());
		List list=q.list();
		if(list!=null && list.size()>0) {
			isValid=true;
		}
		
		return isValid;
	}
}
