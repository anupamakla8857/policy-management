package com.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.model.Admin;

public class AdminDAOImpl implements AdminDAO{

	public void insertAdmin(Admin admin) {
		Session session=new Configuration().configure("admincfg.xml").buildSessionFactory().getCurrentSession();
		
		Transaction tx=session.beginTransaction();
		session.save(admin);
		tx.commit();
	}

	@SuppressWarnings("rawtypes")
	public boolean adminAuthentication(Admin admin) {
		boolean isValid=false;
		Session session=new Configuration().configure("admincfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query q=session.createQuery("from Admin where aid=?1 and password=?2");
		q.setParameter(1,admin.getAid());
		q.setParameter(2,admin.getPassword());
		List list=q.list();
		if(list!=null && list.size()>0) {
			isValid=true;
		}
		
		return isValid;
	}
}
