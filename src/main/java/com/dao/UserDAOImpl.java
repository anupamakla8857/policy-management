package com.dao;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.model.User;                          

public class UserDAOImpl implements UserDAO{

	public void insertUser(User user) {
		Session session=new Configuration().configure("admincfg.xml").buildSessionFactory().getCurrentSession();
		
		Transaction tx=session.beginTransaction();
		session.save(user);
		tx.commit();
	}

	@SuppressWarnings("rawtypes")
	public boolean userAuthentication(User user) {
		boolean isValid=false;
		Session session=new Configuration().configure("admincfg.xml").buildSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query q=session.createQuery("from User where uid=?1 and password=?2");
		q.setParameter(1,user.getUid());
		q.setParameter(2,user.getPassword());
		List list=q.list();
		if(list!=null && list.size()>0) {
			isValid=true;
		}
		
		return isValid;
	}
}
