package com.dao;

import com.model.Vendor;

public interface VendorDAO {
	public boolean vendorAuthentication(Vendor vendor);
	public void insertVendor (Vendor vendor);
}