package com.dao;

import com.model.User;

public interface UserDAO {
	public boolean userAuthentication(User user);
	public void insertUser(User user);
}