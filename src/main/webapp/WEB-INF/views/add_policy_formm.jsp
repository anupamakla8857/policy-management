<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%@include file="./base.jsp"%>

<title>Insert title here</title>
</head>
<body>

	<div class="container mt-3">
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<h1 class="text-center mb-3">Fill the Policy details</h1>
				<form action="handle-policy" method="post">
					<div class="form-group">
						<label for="name">Policy TYpe</label><input type="text"
							class="form-control" name="policyType" id="name">
					</div>

					<div class="form-group">
						<label for="Name">Policy Name</label><input type="text"
							class="form-control" name="policyName" id="name">
					</div>


					<div class="form-group">
						<label for="companyName">companyName</label><input type="text"
							class="form-control" name="companyName" id="companyName">
					</div>

					<div class="form-group">
						<label for="years">years</label><input type="text"
							class="form-control" name="years" id="years">
					</div>

					<div class="form-group">
						<label for="price">price</label><input type="text"
							class="form-control" name="price" id="price">
					</div>
<div class="container text-center">
<a href="${pageContext.request.contextPath }/"
		class="btn btn-outline-danger">Back</a>
		<button type="submit" class="btn-btn-primary">Add </button>
</div>
				</form>
			</div>
		</div>


	</div>

</body>
</html>