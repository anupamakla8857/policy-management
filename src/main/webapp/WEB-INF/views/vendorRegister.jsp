<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Vendor Register</title>
</head>
<body>

<h1>Vendor Registeration</h1>
<hr/>

<a href="index.jsp" />Home</a>
<br/>
<br/>

<s:form action="vendorAdd" method="post"  modelAttribute="e" >

First Name<s:input path="firstName" autocomplete="off" ></s:input>
Last Name<s:input path="lastName" autocomplete="off" ></s:input>
Age<s:input path="age" autocomplete="off" ></s:input>
Gender<s:input path="gender" autocomplete="off" ></s:input>
Contact Number<s:input path="contactNumber" autocomplete="off" ></s:input>
Vendor Id <s:input path="vid" autocomplete="off" ></s:input>
Password<s:input path="password" autocomplete="off" ></s:input>
<s:button>Register</s:button>

</s:form>
</body>
</html>