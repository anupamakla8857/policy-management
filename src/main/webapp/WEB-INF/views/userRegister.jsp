<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Register</title>
</head>
<body>


<h1>User Registeration</h1>
<hr/>

<a href="index.jsp" />Home</a>
<br/>
<br/>

<s:form action="userAdd" method="post"  modelAttribute="u" >

First Name<s:input path="firstName" autocomplete="off" ></s:input>
Last Name<s:input path="lastName" autocomplete="off" ></s:input>
Age<s:input path="age" autocomplete="off" ></s:input>
Gender<s:input path="gender" autocomplete="off" ></s:input>
Contact Number<s:input path="contactNumber" autocomplete="off" ></s:input>
User Id <s:input path="uid" autocomplete="off" ></s:input>
Password<s:input path="password" autocomplete="off" ></s:input>
<s:button>Register</s:button>

</s:form>
</body>
</html>